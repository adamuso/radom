package com.random.radom.radom.utils;

import com.random.radom.radom.events.MiscBus;
import com.random.radom.radom.models.Login;

import org.greenrobot.eventbus.EventBus;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Piotr on 18.03.2016.
 */
public class CommunicationHelper {

    private static String END_POINT = "http://radomcode.cba.pl/";
    private static CommunicationHelper instance;
    private RestAdapter restAdapter;
    private CommunicationService service;
    public String token;

    public static CommunicationHelper getInstance(){
        if(instance == null) {
            instance = new CommunicationHelper();
            instance.restAdapter = new RestAdapter.Builder()
                    .setEndpoint(END_POINT)
                    .build();
            instance.service = instance.restAdapter.create(CommunicationService.class);
        }
        return instance;
    }

    public void login(String username, String password) {
        service.postLogin(username, password, new Callback<Login>() {
            @Override
            public void success(Login login, Response response) {
                instance.token = login.getToken();
                EventBus.getDefault().post(new MiscBus.UserLoggedInEvent());
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
