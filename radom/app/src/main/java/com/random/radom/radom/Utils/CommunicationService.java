package com.random.radom.radom.utils;

import com.random.radom.radom.models.Login;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by Piotr on 18.03.2016.
 */
public interface CommunicationService {
    @POST("/user/login")
    @FormUrlEncoded
    void postLogin(@Field("username") String username, @Field("login") String login, Callback<Login> callback);
}
